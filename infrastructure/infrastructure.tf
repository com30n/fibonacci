terraform {
  backend "gcs" {
    bucket  = "tf-state-eu-north1"
    prefix  = "terraform/state"
  }
}

provider "google" {
  credentials = file("../account.json")
  project = "sturdy-apricot-253607"
  region = "europe-north1"
}

resource "google_container_cluster" "standard-cluster-1" {
  name = "standard-cluster-1"
  location = "europe-north1-a"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}


resource "google_container_node_pool" "node-pool" {
  name = "node-pool"
  location = "europe-north1-a"
  cluster = google_container_cluster.standard-cluster-1.name
  node_count = 1

  node_config {
    preemptible = true
    machine_type = "n1-standard-1"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/trace.append"
    ]
  }
}

resource "google_storage_bucket" helm-charts-storage {
  name = file("./helm-charts-bucket-name")
  location = "EU"
}

data "google_container_registry_repository" "fibonacci" {}

output "gcr_location" {
  value = data.google_container_registry_repository.fibonacci.repository_url
}

//data "helm_repository" "gcs-chars" {
//    name = "gcs-chars"
//    url  = "gs://helm-charts-storage/"
//}
//


