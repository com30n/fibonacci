{{- define "fibonacci.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create unified labels for fibonacci components
*/}}
{{- define "fibonacci.common.matchLabels" -}}
app: {{ template "fibonacci.name" . }}
release: {{ .Release.Name }}
{{- end -}}

{{- define "fibonacci.common.metaLabels" -}}
chart: {{ .Chart.Name }}-{{ .Chart.Version }}
heritage: {{ .Release.Service }}
{{- end -}}



{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fibonacci.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}


{{/*
Return the appropriate apiVersion for networkpolicy.
*/}}
{{- define "fibonacci.networkPolicy.apiVersion" -}}
{{- if semverCompare ">=1.4-0, <1.7-0" .Capabilities.KubeVersion.GitVersion -}}
{{- print "extensions/v1beta1" -}}
{{- else if semverCompare "^1.7-0" .Capabilities.KubeVersion.GitVersion -}}
{{- print "networking.k8s.io/v1" -}}
{{- end -}}
{{- end -}}

{{- define "fibonacci.labels" -}}
{{ include "fibonacci.matchLabels" . }}
{{ include "fibonacci.common.metaLabels" . }}
{{- end -}}

{{- define "fibonacci.matchLabels" -}}
component: {{ .Values.name | quote }}
{{ include "fibonacci.common.matchLabels" . }}
{{- end -}}

{{/*
Create a fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}




{{- define "fibonacci.accounts.labels" -}}
{{ include "fibonacci.accounts.matchLabels" . }}
{{ include "fibonacci.common.metaLabels" . }}
{{- end -}}

{{- define "fibonacci.accounts.matchLabels" -}}
component: {{ .Values.accounts.name | quote }}
{{ include "fibonacci.common.matchLabels" . }}
{{- end -}}

{{/*
Create a fully qualified accounts name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}

{{- define "fibonacci.accounts.fullname" -}}
{{- if .Values.accounts.fullnameOverride -}}
{{- .Values.accounts.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.accounts.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.accounts.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}
