# **Pre-requisites:**
**Google Cloud:**
1. Create free tire account into Google Cloud.
2. Create new project.
3. Create service account for this project with `editor` permissions and download `account.json` with credentials.  
4. Enable Kubernetes API https://console.cloud.google.com/apis/library/container.googleapis.com
5. Enable Container Registry API for your project https://console.cloud.google.com/flows/enableapi?apiid=containerregistry.googleapis.com 

**Gitlab:**
1. Create free account into Gitlab.
2. Fork repository `https://gitlab.com/evgeniy.shubin.android/fibonacci`.
3. Go to `settings -> CI/CD -> variables` and variable `GOOGLE_APPLICATION_CREDENTIALS=./account.json`
**WARNING!!! It can be very dangerous, use Vault or other secret storage instead** 
4. Add your `account.json` into root directory in your repo

# **Prepare infrastructure:**
1. Create GCS bucket, and put it name to `main.tf -> terraform -> backend -> bucket`.
2. Execute `terraform init`.
3. Execute `terraform output` and put value to into `gitlab-ci/general_vaiables.yaml` to `REGISTRY_URL` variable.
4. Auth to gcloud: `gcloud auth activate-service-account --key-file=account.json`
5. Update your `~/.kube/config` by `gcloud container clusters get-credentials standard-cluster-1 --region europe-north1-a`
6. Do `kubectl apply -f infrastructure/helm-rbac.yaml` and then `helm init --service-account=tiller --upgrade`
7. Make a tag to build service images: `git tag infra-0.0.1 && git push --tags`

# **Deploy fibonacci-sequence application to k8s**
1. Create new tag: `git tag 0.0.1 && git push --tags`
2. Go to the `Gitlab -> your repo -> Pipelines` and find pipeline which started from tag `0.0.1`
3. Wait until the pipeline pause on manual action stage
4. Click on the "play" button in the "production" stage
5. Wait until the job has completed successfully

After then execute in your terminal:
```
export SERVICE_IP=$(kubectl get svc --namespace fibonacci fibonacci-sequence -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo http://$SERVICE_IP/docs
```

Go to the link and here you can find fibonacci-sequence service.
