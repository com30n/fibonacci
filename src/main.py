from fastapi import FastAPI
from fibonacci import fibonacci

from starlette_prometheus import PrometheusMiddleware, metrics

app = FastAPI()

app.add_middleware(PrometheusMiddleware)
app.add_route("/-/metrics", metrics, methods=["GET"], include_in_schema=True)


@app.get("/-/ping")
async def ping() -> str:
    """
    Healthcheck, just Healthcheck.
    """
    return "ok"


@app.get("/api/v1/get/{fibonacci_number}")
async def get_fibonacci_number(fibonacci_number: int) -> str:
    """
    Calculation the Fibonacci number and get the result.
    """
    return await fibonacci(fibonacci_number)
