import async_lru


@async_lru.alru_cache()
async def fibonacci(number: int) -> str:
    """
    Calculate fibonacci number for the "n".
    """
    # is_negative - dirty hack for the calculation fibonacci number for negative "n"
    is_negative = False
    if number < 0:
        is_negative = True
        # the calculation for a negative number is the same as for a positive number,
        # but you have to convert "n" to a positive before the calculation and then to a negative in the final
        number = -1 * number

    # the main calculation
    result, accumulator = 0, 1
    for _ in range(number):
        result, accumulator = accumulator, result + accumulator

    if not is_negative:
        return str(result)
    else:
        # if "n" was negative number, convert result to a negative number
        return str(result * -1)
